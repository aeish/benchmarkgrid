import numpy as np
from openpyxl import load_workbook

node = 'LAN'
params = {'node_60kV' : node, # Node name in the 60kV network
          'data_loc': '..\\data\\',  # location for all the data
          'dinemo_loc': '..\\data\\DiNeMo Data\\', # location for DiNeMo Data
          'corres_loc':'..\\data\\corres_data\\', # corres data location
          'netvind_loc':'..\\data\\netvind_loads_and_network\\', # netvind data location
          'plots_loc': 'C:\\workingDirectory\\Programs\\benchmarkgrid\\plots\\ERA5\\'}

dinemo_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+ 'MVLVSubstation.csv' # Data for DiNeMo.
mvlv_substation = len(np.genfromtxt(dinemo_filename, dtype=['f8'], delimiter=',', skip_header=True, usecols=[2]).tolist())
dinemo_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+ 'HVMVSubstation.csv' # Data for DiNeMo.
hvmv_substation = len(np.genfromtxt(dinemo_filename, dtype=['f8'], delimiter=',', skip_header=True, usecols=[2]).tolist())
dinemo_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+ 'CElectricalLines.csv' # Data for DiNeMo.
lines = np.array(np.genfromtxt(dinemo_filename, dtype=['f8','f8'], delimiter=',', skip_header=True, usecols=[4,5]).tolist())
lv_line_length = sum(lines[lines[:,0]==0.4, 1])
mv_line_length = sum(lines[lines[:,0]==20, 1])

wb = load_workbook('.\\results\\benchmark_model_ERA5.xlsx')
ws = wb['Stats']
names = ws['A']
keep_looking = True
k = 0
while keep_looking:
    if names[k].value == params['node_60kV']:
        keep_looking = False
    else:
        k = k+1
k = k+1

ws_stat = wb['Stats']
PV_capacity = ws_stat['F'+str(k)].value
# Finally save
wb.save('.\\results\\benchmark_model_ERA5.xlsx')
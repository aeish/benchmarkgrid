# BenchmarkGrid
**********************************************************
******************* Welcome ******************************
**********************************************************
Title                                          ***********
*****  DTU 7k Bus Active Distribution Network  ***********
**********************************************************
Abbreviation:                                  ***********
*****        DTU-ADN                           ***********
**********************************************************

*************************************************************
******** Authors:                                          
******** 1. Aeishwarya Baviskar                            	
********	Ph.D. Student, Department of Wind Energy       
********    Technical University of Denmark                
********    Email: aeish@dtu.dk			          
********    Phone: +45 93511921			          
********						   
******** 2. Anca D. Hansen				   
********    Associate Professor, Department of Wind Energy 
********    Technical University of Denmark                
********						   
******** 3. Kaushik Das                                    
********    Researcher, Department of Wind Energy            
********    Technical University of Denmark                
********						   
******** 4. Matti Kovisito                                 
********    Researcher, Department of Wind Energy          
********    Technical University of Denmark                
*************************************************************

*************************************************************
Cite this collection: 

Baviskar, Aeishwarya Umesh; Hansen, Anca Daniela; Das, Kaushik; 
Koivisto, Matti Juhani (2021): DTU 7k-Bus Active Distribution 
Network. Technical University of Denmark. 
Collection. https://doi.org/10.11583/DTU.c.5389910
*************************************************************

*************************************************************
Associated Publications:  ***********************************
#### To be updated
*************************************************************

*************************************************************
Keywords: 3 phase balanced distribution network model        
          Multi-voltage level distribution network model    
	  Active distribution network model                 
	  On-shore wind power plants  		            
	  Photovoltaic (PV) power plants 	            
	  Load time series 			            
	  generation time series  		        
	  Network topology   			        
	  High share of renewable energy sources 
	  Renewable energy generation at low voltage level  
*************************************************************

*********************************************************************
Data Description: 						         				
	The DTU-7k Bus Active Distribution Network is a  		 	
	multi-voltage level distribution grid model spanning across	 	
	three voltage levels, namely 60 kV-10 kV-0.4 kV. 		  
	The network is representative of a distribution network with
	a large share of weather-dependent generation connected at 		
	the lower voltage levels such as 10 kV and 400 V. 		  
	The DTU-ADN is developed considering the correlations 		  
	between weather-dependent generation on the load demand. 	  	

	The DTU-ADN is in a format that can be directlt implemented       
	in Python using Pypower or PandaPower, or in MATLAB using         
	MATPOWER.
	
	For more information on the format visit:                         
	1. Pypower: https://rwl.github.io/PYPOWER/api/pypower-module.html 
	2. PandaPower: http://www.pandapower.org/                         
	3. MATPOWER: https://matpower.org/                                
*****************************************************************************

*********************************************************************
*********************************************************************
Files Included in the collection: 

The dataset collection contains three type of datasets

1. Grid-Topology: 
   contains the following files with given formats. 
		i. bus description: https://rwl.github.io/PYPOWER/api/pypower.idx_bus-module.html
		ii. generator description: https://rwl.github.io/PYPOWER/api/pypower.idx_gen-module.html
		iii. distribution line information (with transformers):
				https://rwl.github.io/PYPOWER/api/pypower.idx_brch-module.html
		iv. transformer description: https://pandapower.readthedocs.io/en/v2.6.0/elements/trafo.html#

	Associated Network topology datasets
	> 60 kV Grid-Topology: 
		bus_60kV.csv 
		gen_60kV.csv
		line_60kV.csv
		line_radial_baseMVA100.csv
		trafo_impedance.csv
	
	> 10kV-400V Grid Topologies:
		bus_xx.csv
		gen_xx.csv
		line_xx.csv
		mvlv_substationxx.csv (Note: Transformer files)
	xx in the 10kV-400V Grid Topologies refers to the 60kV node associated with the 
	10kV-400V network. 
	Possible values of xx
	xx: {26, 27, 28, 29, 31, 34, 37, 38, 39, 42, 43, 45, 46, 47}

2. Load and Generation time-series data:
	Load time-series
		> active and reactive power at 1 hour resolution
		> aggregated time-series at 60 kV-10 kV substation
		> individual load time-series at 10 kV or 400 V nodes
		> 27 different load profiles grouped in to
		  household, commercial, agricultural and miscellaneous 
	Generation time-series
		> active power at 1 hour resolution
		> Wind and solar generation time-series from meteorological data
	
	Associated datasets: 
	> Aggregated time-series at 10 kV for the 60 kV network
		- 60 kV network can be simulated using aggregated time-series
		  witout connecting any of the 10kV-400V networks
		- contains 23 csv files with names: xx.csv 
		  where xx: {24, 26, 27, 28, 29, 30,
					 31, 32, 33, 34, 35, 36,
					 37, 38, 39, 40, 41, 42
                     43, 44, 45, 46, 47}
	> WPP generation time-series-60kV network
		- generation profile for the three wind power plants 
		  connected at 60kV level
	> Generation and Load time-series data for 10kV-400V networks
	    - generation and load time-series at 10kV and 400V nodes
		  for the 10kV-400V networks
		- 27 different types of load time-series 
		  not all kinds of time-series included for every network
		- contains 28 csv files named, 
		  load_profile_xx.csv
		  gen_profile_xx.csv 
		  xx: {26, 27, 28, 29, 31, 34, 37, 38, 39, 42, 43, 45, 46, 47}
		  
3. Generation and Load Profile Type 10kV-400V Network
	- type of generation and load profile corresponding to each node
	  in the 10kV-400V network
	- time-series for the load type can be found in the dataset
	  'Generation and Load time-series data for 10kV-400V networks'
	- contains 28 csv files named, 
	  load_xx.csv
	  gen_type_xx.csv
	  xx: {26, 27, 28, 29, 31, 34, 37, 38, 39, 42, 43, 45, 46, 47}
*********************************************************************
*********************************************************************


import numpy as np
import pandas as pd
from pypower.api import makeYbus, dSbus_dV, bustypes
from pypower.idx_bus import VM, VA, PD, QD, BUS_I, ZONE, BUS_TYPE
from pypower.idx_gen import GEN_STATUS, GEN_BUS, VG, PG, PMAX, PMIN, PC1, PC2, QC2MIN, QC1MIN, QC1MAX, QC2MAX, GEN_BUS, \
    QC2MIN, QMAX, QMIN
from pypower.idx_brch import TAP, BR_B, BR_R, BR_X, F_BUS, T_BUS
from pypower.api import ppoption
from pypower_functions import runpf
import pickle as pkl
from datetime import datetime
# TODO 1: Include min and max reactive power limits for generators.
# TODO 2: Automate for multiple timestamps.
# TODO 3: Include provision to incorporate multiple lv networks
''' 
Function definitions. 
'''
def get_type():
    return 0

def specify_bus_loads(bus, bus_load, load_bus_names, _time):
    '''
    input:
        mpc = [dictionary] matpower network
        bus_load = [dictionary] data for bus loads
        load_bus_names = [array] list of load buses
        timestamp = [datetime]
    order of load data = [MVAR, MW, kV]
    output
        mpc = [dict] with new load values are specified at timestamps
    '''
    no_load_buses = len(load_bus_names)
    vm = np.zeros(no_load_buses+1)
    for il, ik in enumerate(load_bus_names):
        load_bus = str(ik)
        bus[ik, PD] = bus_load[load_bus][_time, 1]     # p MW
        bus[ik, QD] = bus_load[load_bus][_time, 0]     # MVAR
        vm[il] = bus_load[load_bus][_time, 2]          # specified voltage magnitude at bus
    vm[-1] = bus_load['24'][_time, 2]
    return bus, vm

def specify_gen(gen, gen_data, gen_bus_names, kv_base, _time):
    '''
    input:
        mpc = [dictionary] matpower network
        gen_data = [dictionary] data for generators
        gen_bus_names = [array] list of gen buses
        timestamp = [datetime]
    order of load data = [MW, kV]
    output
        mpc = [dict] with new gen values are specified at timestamps
    '''
    for il, ik in enumerate(gen_bus_names):
        gen[il+1, PG] = gen_data[str(ik)][_time, 0]
        gen[il+1, VG] = gen_data[str(ik)][_time, 1]/kv_base
    return gen



def gen_timeseries(_params, _busno, timestamp, _net=0):
    if _busno>47:
        _type_file = np.genfromtxt(_params['load_gen_type']+'gen_type_'+str(_net)+'.csv', delimiter=',',
                                   skip_header=True, dtype=['float', 'float', 'U12'], usecols=[0, 2, 3]).tolist()
        _bus_list = np.array([list(item)[0:2] for item in _type_file])
        _idx = np.where(_bus_list == _busno)[0][0]
        _gen_type = _type_file[_idx][2]   # get the slp assigned to this load.
        p = _bus_list[_idx, 1]
        #_gen_type = _gen_type.replace('PP', 'T_')
        _gen_location = np.genfromtxt(_params['timeseries_400V']+'gen_profile_'+str(_net)+'.csv', delimiter=',',
                                      max_rows=1, dtype='U4')
        if _gen_type == 'PV':
            _gen_idx = 1
        else:
            _gen_idx = np.where(_gen_location == _gen_type[0:4])[0]
        _ts_file = np.genfromtxt(_params['timeseries_400V']+'gen_profile_'+str(_net)+'.csv', delimiter=',',
                                 usecols=_gen_idx, skip_header=True)

    return p*_ts_file[timestamp]


'''
Start of the Main File
'''
# Load network

params = dict(net_60kV='results\\Data\\network_60kV\\',
              net_10kV400V='results\\Data\\network_10kV_400V\\',
              timeseries_agg10kV='results\\Data\\timeseries_aggregated_10kV\\',
              timeseries_400V='results\\Data\\timeseries_400V\\',
              load_gen_type='results\\Data\\network_10kV_400V\\load_and_gen_type\\',
              base_MVA=100,
              network_to_attach=np.array([46]),   # USER INPUT
              load_bus_names=np.linspace(26, 47, num=(48-26), endpoint=True,dtype=int),
              gen_bus=[19, 20, 21],
              start_time=datetime(year=2015, month=8, day=8, hour=10, minute=0),
              v_baseLV2_kv=0.4,
              v_baseLV1_kv=10,
              v_baseMV_kv=60,
              v_baseHV_kv=150
              )

# making pypower 60kV network
print('Loading 60kV network...')
filename = params['net_60kV'] + 'bus_60kV.csv'
bus = np.genfromtxt(filename, delimiter=',')
bus[:, BUS_I] = bus[:, BUS_I]-1

filename = params['net_60kV'] + 'gen_60kV.csv'
gen = np.genfromtxt(filename, delimiter=',')
gen[:, GEN_BUS] = gen[:, GEN_BUS]-1


filename = params['net_60kV'] + 'line_radial_baseMVA100.csv'
line = np.genfromtxt(filename, delimiter=',')
line[:, 0] = line[:, 0]-1
line[:, 1] = line[:, 1]-1

# updating 60kV bus nodes with P and Q values
print('updating 60kV network with appropriate P,Q values for given time-instance')

load_bus_names = np.hstack([24, params['load_bus_names']])
load_profiles_60kV = {}
#timestamp = params['start_time']
_filename = params['timeseries_agg10kV']+'24.csv'
timestamps = np.array(np.genfromtxt(_filename, delimiter=',', usecols=[0],
                      dtype=['datetime64[s]'],skip_header=1).tolist())
time_instance = np.where(timestamps == params['start_time'])[0][0]

for i in load_bus_names:
    ii: str = str(i)
    filename = params['timeseries_agg10kV']+str(ii)+'.csv'
    _load_temp = np.array(np.genfromtxt(filename, delimiter=',',
                                        usecols=[1, 2, 3], skip_header=1).tolist())
    load_profiles_60kV[str(ii)] = _load_temp
del _load_temp
del load_bus_names
bus, vm = specify_bus_loads(bus, load_profiles_60kV, params['load_bus_names'], time_instance)
# add appropriate load and generation at the said time instance:

_network_no = str(params['network_to_attach'][0])

bus[bus[:, BUS_I] == int(_network_no), 2:4] = 0

'''
Generator: 
'''
gen_data = {}
k = 19
for i in range(3):
    filename = params['timeseries_agg10kV']+'gen_profile_'+str(k)+'.csv'
    #
    gen_temp = np.array((np.genfromtxt(filename, delimiter=',',
                                       dtype=['f8','f8'],usecols = [1,2],skip_header = 1,unpack=True).tolist()))
    gen_data[str(k)] = gen_temp
    k = k+1
del k
gen = specify_gen(gen, gen_data, params['gen_bus'], params['v_baseLV1_kv'], time_instance)
print('Connecting 10kV-400V network...')

# load 10kV-400V network

filename = params['net_10kV400V'] + 'bus_' + _network_no + '.csv'
_bus = np.genfromtxt(filename, delimiter=',', skip_header=1)

filename = params['net_10kV400V'] + 'gen_' + _network_no + '.csv'
_gen = np.genfromtxt(filename, delimiter=',', skip_header=1)

filename = params['net_10kV400V'] + 'line_' + _network_no + '.csv'
_line = np.genfromtxt(filename, delimiter=',', skip_header=1)

# Join 10kV network and 60kV network


line = np.concatenate((line, _line), axis=0)




# there is only one slack bus in MATPOWER ideally

#### Loading the load files?
_type_file_lv = pd.read_csv(params['load_gen_type'] + 'load_' + _network_no + '.csv')
_slp = pd.read_csv(params['timeseries_400V'] + 'load_profile_' + str(_network_no) + '.csv')

_slps_included = np.unique(_type_file_lv.loc[:,'slp'].values)

_slp_profile_columns = _slp.columns.values.copy()
_slp_profile_columns = np.array([kk[0:4] for kk in _slp_profile_columns])
_slp_profile_columns = _slp_profile_columns[1:]

_mw = _type_file_lv.loc[:, 'mw'].values.copy()
_mvar = _type_file_lv.loc[:, 'mvar'].values.copy()
_node = _type_file_lv.loc[:, 'Node'].values.copy()

for i in range(len(_slps_included)):
    __slp = _slps_included[i]
    _idx = np.where(_slp_profile_columns == __slp)[0]
    if (__slp == 'residual') or (len(_idx) == 0):
        p, q = 0, 0
    else:
        _idx = _idx[0]
        p, q = _slp.iloc[time_instance, _idx+1], _slp.iloc[time_instance, _idx+2]

        _loc_slp = _type_file_lv.loc[:, 'slp'].values == __slp
        _mw[_loc_slp], _mvar[_loc_slp] = _mw[_loc_slp]*p, _mvar[_loc_slp]*q

_sort_idx = _node.argsort()
_out = _sort_idx[np.searchsorted(_node, _bus[:, BUS_I], sorter= _sort_idx)]

_bus[:, PD], _bus[:, QD] = _mw[_out], _mvar[_out]

# combine 60kV buses with LV buses.
bus = np.concatenate((bus, _bus), axis=0)

del __slp, _idx, _mvar, _mw, _loc_slp, _node

# GEN
_gen_type = pd.read_csv(params['load_gen_type']+'gen_type_'+_network_no+'.csv')
_gen_profile = pd.read_csv(params['timeseries_400V']+'gen_profile_'+_network_no+'.csv')

_gen_profile_column = _gen_profile.columns.values.copy()
_gen_profile_column = _gen_profile_column[1:]
_gen_profile_column = np.array([kk[:-3] for kk in _gen_profile_column])

_gen_unique = np.unique(_gen_type.loc[:, 'gen_type'].values)

_gen_mw = _gen_type.loc[:,'mw'].values.copy()
_node = _gen_type.loc[:,'Node'].values.copy()

for i in _gen_unique:
    _idx = np.where(_gen_profile_column == i)[0][0]+1
    p = _gen_profile.iloc[time_instance, _idx]
    _loc_gen = _gen_type.loc[:, 'gen_type'] == i
    _gen_mw[_loc_gen] = _gen_mw[_loc_gen]*p

_sort_idx = _node.argsort()
_out = _sort_idx[np.searchsorted(_node, _gen[:, GEN_BUS], sorter= _sort_idx)]

_gen[:, PG] = _gen_mw[_out]

gen = np.concatenate((gen, _gen), axis=0)

# Setting reactive power limits:
# PF = 0.95 # bare minimum according to grid codes.
gen[1:, QMAX] = gen[1:, PG]*0.33
gen[1:, QMIN] = gen[1:, PG]*-0.33


# run power flow
mpc = dict(version=2, baseMVA=100, bus=bus, gen=gen, branch=line)
opt = ppoption(PF_ALG=1, ENFORCE_Q_LIMS=2, VERBOSE=0, OUT_ALL=1)
print('> Starting Load Flow... \n')
mpc = runpf(mpc, opt)[0]

#dict_mpc = mpc[0]
#f = open("C:\\workingDirectory\\programs\\benchmarkgrid\\results\\data_to_plot\\ORU_ts"+str(time_instance)+".pkl", "wb")
#pkl.dump(dict_mpc, f)
#f.close()

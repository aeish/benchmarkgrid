import numpy as np
import geopandas as gpd
import pandas as pd
from dtw import *
from scipy.optimize import minimize
from docplex.mp.model import Model
from math import floor, ceil
from datetime import datetime
from scipy.stats import pearsonr, spearmanr
import matplotlib.pyplot as plt
import seaborn as sns
from run_profileAllocation import unconstrained_unbounded, bounded_objective, bounded
from bokeh.plotting import figure, output_file, show
from bokeh.layouts import column, row
from my_color_scheme import color_scheme
from openpyxl import load_workbook
import math
import csv

node_60kV_name = [ 'KRE','GLO','STY','VND','BLL','GNG','LAN','LMS','HOL','RAV','RSM','RSL','DUR','SPK','SVL', 'EGR','EGB', 'HDP', 'LOG', 'ORU']
for node in node_60kV_name:
    params = {'node_60kV' : node, # Node name in the 60kV network
                  'data_loc': '..\\data\\',  # location for all the data
                  'dinemo_loc': '..\\data\\DiNeMo Data\\', # location for DiNeMo Data
                  'corres_loc':'..\\data\\corres_data\\', # corres data location
                  'netvind_loc':'..\\data\\netvind_loads_and_network\\', # netvind data location
                  'plots_loc': 'C:\\workingDirectory\\Programs\\benchmarkgrid\\plots\\ERA5\\'}

    dinemo_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+ 'MVLVSubstation.csv' # Data for DiNeMo.
    mvlv_substation = len(np.genfromtxt(dinemo_filename, dtype=['f8'], delimiter=',', skip_header=True, usecols=[2]).tolist())
    dinemo_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+ 'HVMVSubstation.csv' # Data for DiNeMo.
    hvmv_substation = len(np.genfromtxt(dinemo_filename, dtype=['f8'], delimiter=',', skip_header=True, usecols=[2]).tolist())
    dinemo_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+ 'CElectricalLines.csv' # Data for DiNeMo.
    lines = np.array(np.genfromtxt(dinemo_filename, dtype=['f8','f8'], delimiter=',', skip_header=True, usecols=[4,5]).tolist())
    lv_line_length = sum(lines[lines[:,0]==0.4, 1])
    mv_line_length = sum(lines[lines[:,0]==20, 1])

    wb = load_workbook('.\\results\\benchmark_model_ERA5.xlsx')
    ws = wb['Stats']
    names = ws['A']
    keep_looking = True
    k = 0
    while keep_looking:
        if names[k].value == params['node_60kV']:
            keep_looking = False
        else:
            k = k+1
    k = k+1

    ws_stat = wb['Stats']
    ws_stat['P'+str(k)] = mvlv_substation
    ws_stat['Q'+str(k)] = hvmv_substation
    ws_stat['R'+str(k)] = lv_line_length
    ws_stat['S'+str(k)] = mv_line_length

    # Finally save
    wb.save('.\\results\\benchmark_model_ERA5.xlsx')




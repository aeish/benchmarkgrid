from openpyxl import load_workbook
import math
import csv
import numpy as np
import pandas as pd




'''
    User Defined Functions
'''


def number_generators(loads_df, profile, locations):
    '''
    Help:
    This function numbers the gen matrix in pypower.
    Input:
    loads_df: [DataFrame]
    profile: []
    locations: []
    '''

    filename_gen: str = locations["dinemo_loc"] + locations['node_60kV'] + '\\' + 'gen.csv'
    gen = pd.read_csv(filename_gen)
    no_of_gens = len(gen)
    gen_nodes = gen.Node.values
    bus_nodes = loads_df.Node.values
    bus_node_number = loads_df.Node_number.values
    gen_node_number = []
    for a in range(no_of_gens):
        idx = np.where(bus_nodes == gen_nodes[a])[0][0]
        gen_node_number.append(bus_node_number[idx])
    gen_node_number = np.array(gen_node_number)
    gen.Node = gen_node_number
    gen.drop(columns=['Code'], inplace=True)

    '''
            IDX GEN : Pypower
            GEN_BUS | PG | QG | QMAX | QMIN | VG | MBASE (MVA) | GEN_STATUS | PMAX | PMIN | PC1 | PC2 | QC1MIN | QC1MAX |QC2MIN | QC2MAX | RAMP_AGC | RAMS_10 | RAMP_30 | RAMP_Q | APF 
    '''
    empty_gen = np.zeros(no_of_gens)
    ones_gen = np.ones(no_of_gens)
    _sort_idx = np.argsort(gen.Node.values)
    gen_pp = {'GEN_BUS': gen.Node.values[_sort_idx], 'PG': gen.mw.values[_sort_idx],
              'QG': empty_gen, 'QMIN': empty_gen, 'QMAX': empty_gen,
              'VG': ones_gen, 'MBASE': gen.mw, 'GEN_STATUS': ones_gen,
              'PMAX': gen.mw.values[_sort_idx], 'PMIN': empty_gen, 'PC1': empty_gen, 'PC2':empty_gen,
              'QC1MIN': empty_gen, 'QC1MAX': empty_gen, 'QC2MIN': empty_gen, 'QC2MAX': empty_gen,
              'RAMP_AGC': empty_gen, 'RAMS_10': empty_gen,  'RAMP_30': empty_gen, 'RAMP_Q': empty_gen, 'APF': empty_gen}

    gen_pp = pd.DataFrame(gen_pp)
    # filename_lines: str = params["dinemo_loc"] + params['node_60kV'] + '\\' + 'CElectricalLines.csv'
    # gen.rename(columns={'Node_number': 'Node'}, inplace=True)
    gen_type_to_save = params['data_to_save_loc'] + 'gen_type_' + profile + '.csv'
    gen.to_csv(gen_type_to_save, index=False)
    gen_to_save = params['data_to_save_loc'] + 'gen_' + profile + '.csv'
    gen_pp.to_csv(gen_to_save, index=False)
    return 0

def number_lines(loads_df, profile, locations):
    '''
    Help:
    This function numbers the line matrix
    Input:
    loads_df: [DataFrame]
    profile: []
    locations: []
    '''
    _base_MVA = 100
    filename_mvlv: str = locations["dinemo_loc"] + locations['node_60kV'] + '\\' + 'MVLVSubstation_modified.csv'
    mvlv = pd.read_csv(filename_mvlv)
    filename_lines: str = locations["dinemo_loc"] + locations['node_60kV'] + '\\' + 'CElectricalLines.csv'
    lines_dinemo = pd.read_csv(filename_lines)

    nodeA_mvlv = mvlv.NodeA.values
    nodeB_mvlv = mvlv.NodeB.values
    nodeA_mvlv_no = mvlv.BusNoA.values
    nodeB_mvlv_no = mvlv.BusNoB.values
    bus_nodes = loads_df.Node.values
    bus_node_number = loads_df.Node_number.values


    node_names = np.concatenate((np.array(['SSEE0_20.00']),nodeA_mvlv, nodeB_mvlv, bus_nodes), axis=0)
    node_names = np.array([a.strip() for a in node_names])
    node_number = np.concatenate((np.array([int(profile)]),nodeA_mvlv_no, nodeB_mvlv_no, bus_node_number), axis=0)

    del nodeA_mvlv, nodeB_mvlv, nodeA_mvlv_no, nodeB_mvlv_no, bus_nodes, bus_node_number

    nodeA_lines = lines_dinemo.NodeA.values
    nodeB_lines = lines_dinemo.NodeB.values

    nodeA_number = np.zeros(nodeA_lines.shape)
    nodeB_number = np.zeros(nodeB_lines.shape)

    for jj in range(len(nodeA_lines)):
        _idx = np.where(node_names == nodeA_lines[jj].strip())[0][0]
        nodeA_number[jj] = node_number[_idx]
        _idx = np.where(node_names == nodeB_lines[jj].strip())[0][0]
        nodeB_number[jj] = node_number[_idx]

    lines_dinemo.insert(2, 'BusNoA', nodeA_number)  # insert in appropriate places
    lines_dinemo.insert(4, 'BusNoB', nodeB_number)
    lines_dinemo.drop(columns=lines_dinemo.columns.values[-1], inplace=True)

    filename_lines_to_save: str = locations["dinemo_loc"] + locations['node_60kV'] + '\\' + 'lines_modified.csv'
    lines_dinemo.to_csv(filename_lines_to_save, index=False)

    no_of_lines = len(lines_dinemo)
    voltage = lines_dinemo.NVolt_kV.values
    voltage[voltage == 20.0] = 10
    Z_base = voltage**2/_base_MVA
    R_pu = lines_dinemo.R_ohm.values.copy()/Z_base
    Z_pu = lines_dinemo.X_ohm.values.copy()/Z_base

    '''
    IDX_BRCH
    F_BUS | T_BUS | BR_R | BR_X | BR_B | RATE_A | RATE_B | RATE_C | TAP | SHIFT | BR_STATUS | ANGMIN | ANGMAX
    '''
    empty_lines = np.zeros(no_of_lines+mvlv.shape[0])
    ones_lines = np.ones(no_of_lines+mvlv.shape[0])
    TAP = np.concatenate((np.zeros(no_of_lines), np.ones(mvlv.shape[0])), axis=0)
    F_bus = np.concatenate((nodeA_number, mvlv.BusNoA.values), axis=0)
    sort_idx = np.argsort(F_bus)


    line = {'F_BUS': F_bus[sort_idx],
            'T_BUS': np.concatenate((nodeB_number, mvlv.BusNoB.values), axis=0)[sort_idx],
            'BR_R': np.concatenate((R_pu, mvlv.r_pu.values.copy()), axis=0)[sort_idx],
            'BR_X': np.concatenate((Z_pu, mvlv.x_pu.values.copy()), axis=0)[sort_idx],
            'BR_B': empty_lines, 'RATE_A': empty_lines, 'RATE_B': empty_lines,
            'RATE_C': empty_lines, 'TAP': TAP[sort_idx], 'SHIFT': empty_lines,
            'BR_STATUS': ones_lines, 'ANGMIN': -360*ones_lines,
            'ANGMAX': 360*ones_lines}
    line_df = pd.DataFrame(line)
    line_to_save = params['data_to_save_loc'] + 'line_' + profile + '.csv'
    # line_df.to_csv(line_to_save, index=False)
    _ret = np.zeros((len(voltage), 3))
    _ret[:,0], _ret[:,1], _ret[:,2] = voltage, nodeA_number, nodeB_number
    _ret = np.unique(np.concatenate((_ret[:,[0,1]], _ret[:,[0,2]]), axis=0), axis=0)
    return _ret



########################################################################################################################
'''
    Main code starts here. 
'''

nameVsProfile = np.array(np.genfromtxt('..\\data\\name_to_profile.txt', delimiter=',', dtype=['S16, f8']).tolist())  # array of bus name and assigned number.

# node_60kV_name = ['HOL']
node_60kV_name = ['GLO', 'ORU', 'HOL', 'KRE', 'BLL', 'LMS', 'SVL', 'LAN', 'STY', 'GNG', 'VND', 'SPK', 'EGR', 'RAV',
                  'RSM', 'RSL', 'DUR']             # list of all 60kV/10kV buses-

params = dict(node_60kV='GLO',  # node name
              data_loc='..\\data\\',  # all data location
              dinemo_loc='..\\data\\DiNeMo Data\\',  # location for DiNeMo Data
              corres_loc='..\\data\\corres_data\\',  # Location for CorRES data (not used here)
              netvind_loc='..\\data\\netvind_loads_and_network\\',  # Netvind data location
              data_to_save_loc='C:\\workingDirectory\\Programs\\benchmarkgrid\\results\\Data\\',
              plots_loc='C:\\workingDirectory\\Programs\\benchmarkgrid\\plots\\ERA5\\')  # location to save plots.
# Last 10kV node is 47
# GLO then starts from 48-> no of nodes in GLO
#
bus_number = 48 # this is correct
for node in node_60kV_name:
    params['node_60kV'] = node

    # Load the profile number corresponding to the name
    for i in nameVsProfile:
        if i[0][0].decode() == params['node_60kV']:
            profile_no = i[0][1].decode()[:2]
            break
    '''
    # Loading RES profile and saving in appropriate location | No need to repeat operation. 
    filename_res: str = params["dinemo_loc"] + params['node_60kV'] + '\\' + 'res_profile.csv'  # DiNeMo File name
    res_profile = pd.read_csv(filename_res)
    save_file = params['data_to_save_loc'] +'\\timeseries_400V\\gen_profile_' + profile_no + '.csv'
    res_profile.to_csv(save_file, index=False)
    '''
    filename_MVLV: str = params["dinemo_loc"] + params['node_60kV'] + '\\' + 'MVLVSubstation.csv'  # 10kV / 400V substation File name
    MVLV_substation = pd.read_csv(filename_MVLV)                                                   # Reading 10kV / 400V substation file
    if len(MVLV_substation.columns) > 6:
        MVLV_substation.drop(MVLV_substation.columns[[-1]], axis=1, inplace=True)                # Drop excess column -> there is an extra redundant element,
    # MVLV_pp = MVLV_substation.copy()                                                             # copy much?

    nodeA = MVLV_substation.NodeA.values                                                                 # get nodeA names
    nodeB = MVLV_substation.NodeB.values                                                                 # get nodeB names

    nodeA_bus_nos = np.array([int(a[2:]) + bus_number for a in nodeA])                         # calculate nodeA number
    nodeB_bus_nos = nodeA_bus_nos + len(nodeA_bus_nos)                                         # calculate nodeB number

    MVLV_substation.insert(2, 'BusNoA', nodeA_bus_nos)                                         # insert column in appropriate places in the DataFrame
    MVLV_substation.insert(5, 'BusNoB', nodeB_bus_nos)
    MVLV_substation.loc[:, 'NVoltA_kV'] = 10  # change 20kV to 10kV                                          # change MV to 11kV
    MVLV_substation.loc[:, 'NVoltB_kV'] = 0.4  # change 400V to 433V to match manufactured transformer data  # change LV to 433V

    bus_number = max(nodeB_bus_nos) + 1    # Update bus number.
    '''
    This is the structure of the branch / line data in Pypower 
        IDX_BRCH
        F_BUS | T_BUS | BR_X | BR_B | RATE_A | RATE_B | RATE_C | TAP | SHIFT | BR_STATUS | ANGMIN | ANGMAX

        '''
    no_of_transformers = len(nodeA_bus_nos)
    empty_mvlv = np.zeros(no_of_transformers)
    ons_mvlv = np.zeros(no_of_transformers)

    # Transformer data from ABB data sheet:
    # MVA base for the system : 100 MVA
    # Z_base = (10kV)^2/100MVA = 1.
    # Cap | R | X | pfe_kW | io_percent | vk_percent | vkr_percent

    trafo_data_abb = np.array([[0.4, 2.39, 10.76, 0.475, 0.113, 4, 0.23],
                               [0.63, 2.19, 9.26, 0.8, 0.126, 6, 0.236],
                               [1, 1.22, 5.157, 1.3, 0.13, 5.3, 0.236]])
    trafo_params = np.zeros([no_of_transformers,trafo_data_abb.shape[1]])
    capacity = MVLV_substation.Cap_kVA.values.copy()/1000
    trafo_params[capacity == 0.4, :] = trafo_data_abb[0, :]
    trafo_params[capacity == 0.63, :] = trafo_data_abb[1, :]
    trafo_params[capacity == 1, :] = trafo_data_abb[2, :]

    MVLV_substation.insert(8, 'r_pu', trafo_params[:, 1])   # insert column relating the resistance and impedance in pu.
    MVLV_substation.insert(9, 'x_pu', trafo_params[:, 2])

    filename_MVLV: str = params["dinemo_loc"] + params['node_60kV'] + '\\' + 'MVLVSubstation_modified.csv'

    '''
    # Transformer data -> MV LV Substation # Structure in PandaPower?  
    HV_bus | LV_bus | r_pu | x_pu | vkv_base | vn_lv_kv | smva_base | pfe_kw | i0_percent | vk_percent | vkr_percent
    '''

    MVLV_pp = {'hv_bus':  MVLV_substation.BusNoA.values, 'lv_bus': MVLV_substation.BusNoB.values,
               'r_pu': trafo_params[:, 1], 'x_pu': trafo_params[:, 2],
               'vkv_base': MVLV_substation.NVoltA_kV.values, 'vn_lv_kv': MVLV_substation.NVoltB_kV.values,
               'smva_base': capacity,
               'pfe_kw': trafo_params[:, 3], 'i0_percent': trafo_params[:, 4],
               'vk_percent': trafo_params[:, 5], 'vkr_percent': trafo_params[:, 6]}

    MVLV_pp = pd.DataFrame(MVLV_pp)    # Convert to DataFrame to save as csv

    save_file = params['data_to_save_loc']+'mvlv_substation'+profile_no+'.csv'  # save file name
  #  MVLV_pp.to_csv(save_file, index=False)    # uncomment to save file.

    '''
    Load the load profile file and save in the appropriate location  || No need to repeat operation
    '''
    '''
    filename_load_profile: str = params["dinemo_loc"] + params['node_60kV'] + '\\' + 'load_profile.csv'
    load_profile = pd.read_csv(filename_load_profile)
    save_file = params['data_to_save_loc'] + 'load_profile_' + profile_no + '.csv'
    load_profile.to_csv(save_file, index=False)
    '''
print('Current Highest bus number is: ' + str(bus_number))

for node in node_60kV_name:
    params['node_60kV'] = node
    print('Current Node: ' + node)
    for i in nameVsProfile:
        if i[0][0].decode() == params['node_60kV']:
            profile_no = i[0][1].decode()[:2]
            break
    filename_loads: str = params["dinemo_loc"] + params['node_60kV'] + '\\' + 'load.csv'    # contains bus name, P, Q, SLP and bus type
    loads = pd.read_csv(filename_loads)      # read the load file

    node_volts = loads.NVolt_kV.values      # Voltage at each node
    node_names = loads.Node.values          # Node name? BUS NAME

    node_names_lv = node_names[node_volts == 0.4]    # LV nodes
    node_names_hv = node_names[node_volts == 10]     # HV nodes

    node_names_lv = np.array([int(a[3:]) for a in node_names_lv])   # LV node names
    node_names_lv = node_names_lv + bus_number                      # number the LV nodes
    bus_number = max(node_names_lv) + 1                             # update bus_number

    node_names_hv = np.array([int(a[3:]) for a in node_names_hv])   # MV node names
    node_names_hv = node_names_hv + bus_number                      # number MV nodes
    bus_number = max(node_names_hv) + 1                             # update bus number

    number_of_nodes = len(node_names)
    empty_array = np.zeros(number_of_nodes)
    array_of_ones = np.ones(number_of_nodes)

    loads.insert(6, 'Node_number', empty_array)
    loads.loc[node_volts == 0.4, 'Node_number'] = node_names_lv
    loads.loc[node_volts == 10, 'Node_number'] = node_names_hv
    loads.drop(columns='Code', inplace=True)

    _ = number_generators(loads, profile_no, params)
    _line_bus_volt = number_lines(loads, profile_no, params)

    _bus_nodes = loads.Node_number.values.copy()
    nodes_not_in_bus = np.setdiff1d(_line_bus_volt[:,1], _bus_nodes)

    _volt_at_buses = _line_bus_volt[np.isin(_line_bus_volt[:,1], nodes_not_in_bus), 0]
    nodes_not_in_bus = _line_bus_volt[np.isin(_line_bus_volt[:,1], nodes_not_in_bus), 1]

    _idx_sort = np.argsort(nodes_not_in_bus)
    nodes_not_in_bus = nodes_not_in_bus[_idx_sort][1:]
    _volt_at_buses = _volt_at_buses[_idx_sort][1:]
    del _idx_sort

    extra_loads = np.zeros((len(_volt_at_buses), 7))
    extra_loads[:,4], extra_loads[:,5], extra_loads[:,1] = np.ones(len(nodes_not_in_bus)), nodes_not_in_bus, _volt_at_buses
    extra_loads = pd.DataFrame(data=extra_loads, columns=loads.columns)
    del nodes_not_in_bus, _volt_at_buses, _bus_nodes
    loads = pd.concat([loads, extra_loads])

    empty_array = np.zeros(len(loads))
    array_of_ones = np.ones(len(loads))

    '''
    IDX BUS: Pypower
    Bus_I | BUS_TYPE | PD | QD | GS | BS | BUS_AREA | VM | VA | BASE_kV | ZONE | VMAX | VMIN 
    '''
    sort_idx = np.argsort(loads.Node_number.values)
    bus = {'BUS_I': loads.Node_number.values[sort_idx],
           'BUS_TYPE': loads.bus_type.values[sort_idx],
           'PD': loads.mw.values[sort_idx],
           'QD': loads.mvar.values[sort_idx],
           'GS': empty_array, 'BS': empty_array, 'BUS_AREA': empty_array,
           'VM': array_of_ones, 'VA': empty_array,
           'BASE_kV': loads.NVolt_kV.values[sort_idx], 'ZONE': array_of_ones * int(profile_no),
           'VMAX': array_of_ones * 1.1, 'VMIN': array_of_ones * 0.9}
    bus_df = pd.DataFrame(bus)
    load_file_names = params['data_to_save_loc'] + 'load_' + profile_no + '.csv'
    loads.to_csv(load_file_names, index=False)
    bus_file_name = params['data_to_save_loc'] + 'bus_' + profile_no + '.csv'
    bus_df.to_csv(bus_file_name, index=False)


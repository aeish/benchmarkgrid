import numpy as np
import geopandas as gpd
import pandas as pd
from dtw import *
from scipy.optimize import minimize
from docplex.mp.model import Model
from math import floor, ceil
from datetime import datetime
from scipy.stats import pearsonr, spearmanr
import matplotlib.pyplot as plt
import seaborn as sns
from run_profileAllocation import unconstrained_unbounded, bounded_objective, bounded
from bokeh.plotting import figure, output_file, show
from bokeh.layouts import column, row
from my_color_scheme import color_scheme
from openpyxl import load_workbook
import math
import csv
'''
This file is used for the following purposes: 
    1. parameters
    2. loading data. 
'''

#def run_wrapper(node_60kV_name):
params = {'node_60kV':   'DUR',                                                           # Node name in the 60kV network
          'data_loc'   : '..\\data\\',                                                      # location for all the data
          'dinemo_loc' : '..\\data\\DiNeMo Data\\',                                       # location for DiNeMo Data
          'corres_loc' : '..\\data\\corres_data\\',                                        # corres data location
          'netvind_loc': '..\\data\\netvind_loads_and_network\\',                         # netvind data location
          'plots_loc'  : 'C:\\workingDirectory\\Programs\\benchmarkgrid\\plots\\ERA5\\'}   # location to save the plots.
# nameVsProfile has the number of nodes against their names.
nameVsProfile = np.array(np.genfromtxt('..\\data\\name_to_profile.txt', delimiter=',', dtype=['S16, f8']).tolist())
# node_name_list = ['ORU']
node_name_list = ['GLO', 'ORU', 'HOL', 'KRE', 'BLL', 'LMS', 'SVL', 'LAN', 'STY', 'GNG', 'VND', 'SPK', 'EGR', 'RAV',
                  'RSM', 'RSL', 'DUR']
# node_name_list = ['VND']
flag_first_time = True
for node_name in node_name_list:
    print('#######################################################')
    print('Starting new node. '+node_name)
    print('#######################################################')
    params['node_60kV'] = node_name

    solar_profile_no = 0 # solar_profile_no is the column number of the solar profile data for the 60kV node.
    for i in nameVsProfile:
        if i[0][0].decode() == params['node_60kV']:
            profile_no = i[0][1].decode()[:2]
            break
        solar_profile_no = solar_profile_no + 1
    #del nameVsProfile
    '''
    Specify file names with locations
    '''
    # consumer_file= 'Ørum 001\\CConsumer.shp' We shall use this later
    solar_filename = params['corres_loc']+'SOLAR_POW_NEW.csv' # name for the csv file
    wind_filename = params["corres_loc"]+'WPP_POW.csv' # name for the wind profile
    netvind_filename = params["netvind_loc"] + profile_no +'.csv' # netvind data file
    simbench_filename = params['data_loc'] + "simbench_loads_profiles_pu.csv" # simbench data file
    dinemo_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+ 'CConsumer.csv' # Data for DiNeMo. [not used so far]
    #if node_60kV_name =='RSM':
    #    wpp_filename = params["netvind_loc"] + 'wpp19' +'.csv'
    #    cap=12
    #    flag_subtract=1
    #elif node_60kV_name == 'RSL':
    #    wpp_filename = params["netvind_loc"] + 'wpp20' + '.csv'
    #    cap=15
    #    flag_subtract = 1
    #elif node_60kV_name == 'DUR':
    #    wpp_filename = params["netvind_loc"] + 'wpp21' + '.csv'
    #    cap=15
    #    flag_subtract = 1
    #else:
    #    flag_subtract=0
    '''
    Loading Data. 
        1. Netvind Data -> 60kV profiles
        2. Solar and Wind profile from CorRES output
        3. Simbench data. 
    '''
    '''
    #################### load netvind profile
    '''
    netvind_pu = np.array(np.genfromtxt(netvind_filename, delimiter=',', dtype=['datetime64[s]', 'f8', 'f8', 'f8'],skip_header=1, encoding='ascii').tolist())
    netvind_MVA_pu = netvind_pu[:,2] # This is only active power.
    netvind_MVAR = netvind_pu[:,1] # This formula is for active power**2 + reactive power**2
    netvind_MVAR[netvind_MVA_pu==0] = 0
    netvind_MVA_pu[netvind_MVA_pu==0] = 1e-3

    #PF = abs(netvind_MVA_pu/(netvind_MVAR**2+ netvind_MVA_pu**2)**0.5)
    num_timestamps_NV = len(netvind_MVA_pu) # length of timestamps available.

    netvind_timestamps = netvind_pu[:num_timestamps_NV,0] # all the timestamps in netvind data -> 60kV network
    #del del_me
    '''
    ##################  load solar and wind profiles profile
    '''
    dtype_lst = ['f8'] # degined data type as float
    #print(f'solar profile number is {solar_profile_no}')  # check
    solar_pp = np.array(np.genfromtxt(solar_filename, delimiter=',', dtype=dtype_lst, skip_header=1,usecols=[solar_profile_no], encoding='ascii').tolist())


    solar_pp_pu = solar_pp[7*24+10: num_timestamps_NV+(7*24)+10] # the index adjustment is done to start at the same time as the netvind timestamps.

    # get wind profile
    wind = pd.read_csv(wind_filename)
    names = wind.columns.values[:] # long names
    names = np.array([i[0:3] for i in names]) # convert to short 3 letter names
    index_used = np.where(names==params['node_60kV'])[0].tolist() # find the column numbers of the 60kV node wind profile- multiple wind profiles
    wind = wind.iloc[:,index_used].values #
    wind_pp = np.zeros(wind.shape[0]) # initialize the wind_pp array which will be a sum of all the wind profiles.
    for i in range(wind.shape[1]):
        wind_pp = wind_pp + wind[:,i] # add all the wind profiles to form one wind profile
    #wind_pp = wind[:,0]
    #print(f'Number of wind profiles: {wind.shape[1]}') # intermediate check
    #if wind.shape[1] >=1:
    #    wind_pp = wind_pp / wind.shape[1]

    wind_pp = wind[7*24+10: num_timestamps_NV+(7*24)+10] # index adjustment to match netvind timestamps.
    num_of_wind_profiles = wind_pp.shape[1]
    del dtype_lst, wind, index_used
    #load_gen_profiles_pu = {'solar_pu': solar_pp_pu, 'netvind':netvind_MVA_pu, 'simbench_profiles': simbench_load_profiles_index0_MVA_pu}

    '''
    ################## Load simbench reference load profiles in pu
    '''

    num_profiles_from_simbench = 27 # constant
    dtype_pro = ['f8']*num_profiles_from_simbench*2   # There are 25 different load profiles available from Simbench.
    dtype_list = ['S16']
    dtype_list = dtype_list.append(dtype_pro)
    simbench_all_profiles_STR = np.array(np.genfromtxt(simbench_filename, delimiter=';',dtype=dtype_list, encoding=None).tolist()) # simbench profiles.

    len_timestamps_from_simbench = simbench_all_profiles_STR.shape[0] - 1
    simbench_all_load_profiles = np.zeros( [len_timestamps_from_simbench, num_profiles_from_simbench * 2])  # multiply 2 in the number of profiles to account for reactive power.
    simbench_timestamps = [] # initialize list.

    sim_names = simbench_all_profiles_STR[0][:][1:] # Names for simbench profiles.

    for i in range(len_timestamps_from_simbench):
        simbench_timestamps.append(datetime.strptime(simbench_all_profiles_STR[i + 1][0], '%d.%m.%Y %H:%M')) # get simbench timestamps as datetimes
        for j in range(num_profiles_from_simbench * 2):
            simbench_all_load_profiles[i, j] = float(simbench_all_profiles_STR[i + 1][j + 1]) # converting pu value from string to float.

    simbench_load_profiles_MVA_pu = np.zeros([len_timestamps_from_simbench,num_profiles_from_simbench])
    active_power_index = np.linspace(1,num_profiles_from_simbench*2-1, num=num_profiles_from_simbench, dtype=int).tolist()
    reactive_power_index = np.linspace(0,num_profiles_from_simbench*2-2, num=num_profiles_from_simbench, dtype=int).tolist()

    simbench_load_profiles_MVA_pu = simbench_all_load_profiles[:,active_power_index]
    reactive_simbench = math.sin(math.acos(0.93))*simbench_all_load_profiles[:,reactive_power_index]
    #simbench_load_profiles_MVA_pu = (active_simbench**2 + reactive_simbench**2)**0.5
    #for i in range(num_profiles_from_simbench):
        #if i%2 == 0:
        #temp = simbench_all_load_profiles[:,i+1] # this is only active power.
        #temp = np.sqrt(simbench_all_load_profiles[:,i]**2 + simbench_all_load_profiles[:,i+1]**2)
        #simbench_load_profiles_MVA_pu[:, i] = temp # MVA in p.u
    # note: simbench_load_profiles_MVA_pu contains only MW values.

    del len_timestamps_from_simbench, simbench_all_load_profiles, simbench_all_profiles_STR, i, j, dtype_list, dtype_pro

    #simbench_0_index = [i for i in range(len(simbench_timestamps)) if simbench_timestamps[i].minute == 0]
    #simbench_load_profiles_index0_MVA_pu = simbench_load_profiles_MVA_pu[simbench_0_index[:num_timestamps_NV], :]



    ''' 
    Preprocessing Simbench data. 
    
        1. Simbench data is given at 15 minute intervals, whereas, netvind at 1 hour intervals. So we calculate 1 hour averages for simbench data
        2. simbench_timestamps_new = timestamps when minute == 0
        3. Equating Simbench timestamps to netvind with respect to month, date, hour. This also calculates if any data is missing 
        4. find out the indices of all the missing dates
    '''

    # Average over an hour for all simbench timestamps
    num_simbench = len(simbench_timestamps) # number of simbench timestamps
    # separating hour timestamps
    hour_indices = [a for a in range(num_simbench) if simbench_timestamps[a].minute==0] # timestamps for minute == 0
    simbench_timestamps_new  = [simbench_timestamps[hour] for hour in hour_indices]  # simbench hourly timestamps.
    simbench_1_hour_avg = np.zeros([int(num_simbench/4), simbench_load_profiles_MVA_pu.shape[1]]) #initialize array for average values over an hour.
    simbench_1_hour_reactive = np.zeros([int(num_simbench/4), simbench_load_profiles_MVA_pu.shape[1]])
    for j in range(len(hour_indices)):
        for i in range(27):               # Average                     Over 4 timestamps starting from minute == 0
            simbench_1_hour_avg[j,i] = np.mean(simbench_load_profiles_MVA_pu[hour_indices[j]:hour_indices[j]+4, i]) # calculate hour averages.
            simbench_1_hour_reactive[j, i] = np.mean(reactive_simbench[hour_indices[j]:hour_indices[j] + 4, i])
    simbench_timestamps_new = np.array(simbench_timestamps_new) # converting simbench timestamps from list to numpy array

    if flag_first_time == True:
        net_sim_indices = []
        missing_date = []
        for net_date in netvind_timestamps:
            k = 0
            flag_continue = True
            while flag_continue:
                if simbench_timestamps_new[k].month==net_date.month and \
                    simbench_timestamps_new[k].day==net_date.day and \
                    simbench_timestamps_new[k].hour==net_date.hour:
                    net_sim_indices.append(k)
                    flag_continue = False
                else:
                    k = k+1
                    if k == len(simbench_timestamps_new):
                        flag_continue = False
                        print(f"missing {net_date} ")
                        missing_date.append(net_date)

        print(f"No of missing dates: {len(missing_date)}")

        # find out the indices of all the missing dates.
        k = 0
        miss_date_index = []
        for miss_date in missing_date:
            flag_continue = True
            while flag_continue:
                if netvind_timestamps[k].month == miss_date.month and \
                        netvind_timestamps[k].day == miss_date.day and \
                        netvind_timestamps[k].hour == miss_date.hour:

                    miss_date_index.append(k)  # missed date + hour index

                    flag_continue = False
                else:
                    k = k + 1
        del flag_continue

    simbench_timestamps_old = simbench_timestamps_new # store all the simbench timestamps to new location
    simbench_timestamps_new = simbench_timestamps_new[net_sim_indices]

    # Rearrange and select the load profiles for those timestamps.
    simbench_all = simbench_1_hour_avg[net_sim_indices]
    simbench_reactive = simbench_1_hour_reactive[net_sim_indices]

    # Replace missing date with back fill.
    for i in range(len(miss_date_index)):
        simbench_all = np.insert(simbench_all, miss_date_index[i], simbench_all[miss_date_index[i], :], axis=0)
        simbench_reactive = np.insert(simbench_reactive, miss_date_index[i], simbench_reactive[miss_date_index[i], :], axis=0)
    # Reinsert missing date in timestamps-
    simbench_timestamps_new = np.insert(simbench_timestamps_new,  miss_date_index[0],missing_date[0], axis=0)
    #sim_names_full = sim_names
    sim_names =   [sim_names[i][0:4] for i in np.arange(0,len(sim_names),2)]
    sim_classification = [sim_names[i][0:1] for i in np.arange(0,len(sim_names),1)]
    sim_classification = np.array(sim_classification)
    #simbench_reactive = math.cos(0.93)*simbench_reactive
    del reactive_simbench


    '''
    The timeseries dictionary is made to be passed as an argument in the optimization function
    '''
    timeseries = {'netvind_MVA':netvind_MVA_pu ,
                  'wind_pp':wind_pp,
                  'solar_pp':solar_pp_pu,
                  'simbench_load_profiles':simbench_all
                  }
    ###########**********************************************************************************************************************
    '''
    Model 1: unconstrained unbounded
    
    '''
    mdl1 = Model(name='Capacity_estimation_unbounded')
    # The bounds are very large
    num_coef= num_profiles_from_simbench+1+num_of_wind_profiles
    lower_bound_dummy = np.zeros(num_coef)
    upper_bound_dummy = np.ones(num_coef)*100

    # adding variables: only the installed capacities for all the profiles in MW
    coef = mdl1.continuous_var_list(num_coef, lower_bound_dummy, upper_bound_dummy, name='coef')
    solution1 = unconstrained_unbounded(mdl1, coef, timeseries)
    print(f'RMSE Error: {np.sqrt(solution1.objective_value)} MVA')

    # These are the values of MW and MVAR for each load
    sol1 = [solution1.get_value(i) for i in coef] # MW
    sol1_reactive = [0]*(num_of_wind_profiles+1)
    sol1_reactive.extend((np.array(sol1[num_of_wind_profiles+1:])*math.acos(0.97)).tolist()) # MVAR

    gen_at10kV = np.hstack((np.expand_dims(timeseries['solar_pp'][:,0], axis=1), timeseries['wind_pp']))
    load_at10kV = timeseries['simbench_load_profiles']
    res = gen_at10kV @ sol1[0:num_of_wind_profiles + 1]

    netvind_only_load_1 =timeseries['netvind_MVA']
    solution_1_at10kV = load_at10kV@sol1[num_of_wind_profiles+1:] #gen_at10kV@sol1[0:num_of_wind_profiles+1]

    corr_sol1 = pearsonr(timeseries['netvind_MVA'], solution_1_at10kV-res)
    print(f'Correlation between the profiles {corr_sol1}')

    residuals = netvind_only_load_1 - solution_1_at10kV
    '''
    ##################################### Dynamic time warping 
    '''

    query = netvind_only_load_1
    template = solution_1_at10kV-res
    alignment = dtw(query, template, keep_internals=True, dist_method="euclidean")
    # project template with the indices.

    # alignment.plot(type="threeway", figsize=(15,15))
    #plt.show()

    template_DTW = template[alignment.index2]
    template_load_at10kV = load_at10kV[alignment.index2]
    template_reactive_at10kV = simbench_reactive[alignment.index2]
    template_res_gen = gen_at10kV[alignment.index2,:]
    N = len(query)
    DTW_load_profile = np.zeros(query.shape)
    loads10 = np.zeros([N, 27])
    reactive10 = np.zeros([N,27])
    #gen10 = np.zeros(gen_at10kV.shape)

    #PF = timeseries['simbench_PF_profile']

    for i in range(N):
        DTW_load_profile[i] = np.mean(template_DTW[alignment.index1==i])
        for j in range(27):
            loads10[i,j] = np.mean(template_load_at10kV[alignment.index1==i,j])
            reactive10[i,j] = np.mean(template_reactive_at10kV[alignment.index1==i,j])

    DTW_load_profile = loads10@sol1[num_of_wind_profiles+1:]#- gen10@sol1[0:num_of_wind_profiles+1]
    DTW_reactive_profile = reactive10@sol1_reactive[num_of_wind_profiles+1:]

    ########################### Error after DTW:
    RMSE_DTW = np.sqrt(np.mean((netvind_only_load_1-DTW_load_profile+res)**2))
    corr_DTW = pearsonr(timeseries['netvind_MVA'], DTW_load_profile-res)[0]
    residuals_DTW = netvind_only_load_1 - DTW_load_profile+res
    residual_DTW_reactive = netvind_MVAR - DTW_reactive_profile
    '''
    ######### To find out which simbench profiles are available/active in the given network. 
    '''
    sol1_round = np.array([round(i,4) for i in sol1]) # rounding off profile values upto 2 decimal places
    sim_names_arr = np.array(sim_names) # make an array for sim_names
    sol_res = sol1_round[:num_of_wind_profiles+1]
    sol1_round = sol1_round[num_of_wind_profiles+1:] # eliminate solar and wind power capacities

    sim_names_arr = sim_names_arr[sol1_round!=0] # keep profile names with installed power >0
    res_names = ['PV_pu']
    for i in range(1, num_of_wind_profiles+1):
        if sol_res[i] != 0:
            dum_name = 'WPP'+str(i)+'_'+'pu'
            res_names.append(dum_name)

    '''
    ####################   Saving Load and Generation profiles
    '''
    loads10 = loads10[:,sol1_round!=0]
    reactive10 = reactive10[:,sol1_round!=0]
    if sum(sol_res!=0):
        gen10 = gen_at10kV[:, sol_res!=0]
        saveme_res = pd.DataFrame(gen10.tolist(), index=netvind_timestamps, columns=res_names)
        filename_res = params['dinemo_loc'] + params['node_60kV'] + '\\' + 'res_profile.csv'
        csq1 = saveme_res.to_csv(filename_res, index=True)
        del csq1, saveme_res, res_names

    num_of_load_profiles = loads10.shape[1]*2+2
    load_profiles_to_save = np.zeros((loads10.shape[0],num_of_load_profiles))
    even_indices = np.arange(0, num_of_load_profiles,2)
    odd_indices = np.arange(1, num_of_load_profiles,2)
    # add load profile MW and MVAR to array
    load_profiles_to_save[:,even_indices] = np.concatenate((loads10,np.expand_dims(residuals_DTW, axis=1)),axis=1)
    load_profiles_to_save[:,odd_indices] = np.concatenate((reactive10,np.expand_dims(residual_DTW_reactive, axis=1)),axis=1)
    # make column names list
    P_names = [a+'_pu_MW' for a in sim_names_arr]
    P_names.append('residual_pu')
    Q_names = [a+'_pu_MVAR' for a in sim_names_arr]
    Q_names.append('residual_pu')
    names = ['change this']*num_of_load_profiles

    for k in range(int(num_of_load_profiles/2)):
        names[even_indices[k]] = P_names[k]
        names[odd_indices[k]] = Q_names[k]
    saveme = pd.DataFrame(load_profiles_to_save.tolist(), index=netvind_timestamps, columns=names)

    filename = params['dinemo_loc'] + params['node_60kV'] +'\\'+ 'load_profile.csv'


    # csq = saveme.to_csv(filename,index=True) # save file as csv

    del saveme, P_names,Q_names, names, load_profiles_to_save, even_indices, odd_indices
    data_to_save_for_plots = {'orig':timeseries['netvind_MVA'], 'wo_dtw': solution_1_at10kV-res, 'with_dtw':DTW_load_profile-res}
    data_to_save_for_plots = pd.DataFrame(data_to_save_for_plots)
    data_to_save_for_plots.to_csv('C:\\workingDirectory\\Programs\\benchmarkgrid\\results\\data_to_plot\\'+params['node_60kV'], index=False)
    '''
    #################################################### Plots
    '''
    node_name = params['node_60kV']
    mycolor = color_scheme()
    color_q = mycolor['qualitative']
    color_seq = mycolor['sequential']['m1']
    ### ------------------------------Time series plot
    timeseries_plot_name = params["node_60kV"]+"_timeseries.html"
    output_file(params['plots_loc']+timeseries_plot_name)
    # create new plot with a title and axis labels
    p = figure(title='Load time-series after DTW', x_axis_label='time', y_axis_label='Load [MW]', plot_width=1500)
    # add a line renderer with legend and line thickness

    p.line(range(len(timeseries['netvind_MVA'])),timeseries['netvind_MVA'], legend= 'Netvind',line_color=color_q[0])
    p.circle(range(len(timeseries['netvind_MVA'])),timeseries['netvind_MVA'], legend= 'Netvind',color=color_q[0])
    p.line(range(len(solution_1_at10kV)),solution_1_at10kV-res, legend='Simbench-RES', line_color=color_q[1])
    p.triangle(range(len(solution_1_at10kV)),solution_1_at10kV-res, legend='Simbench-RES', color=color_q[1])
    p.line(range(N),DTW_load_profile-res, legend='DTW profile-RES', line_color=color_q[2])
    p.square(range(N),DTW_load_profile-res, legend='DTW profile-RES', color=color_q[2])
    p.xaxis.axis_label_text_font_size = "15pt"
    p.yaxis.axis_label_text_font_size = "15pt"
    p.legend.label_text_font_size = "15pt"
    p.xaxis.major_label_text_font_size = "12pt"
    p1 = figure(title='Renewable energy profile', x_axis_label='time', y_axis_label='Generation [MW]', plot_width=1500)
    p1.line(range(N), res, line_color=color_q[2], legend='without DTW')
    p1.xaxis.axis_label_text_font_size = "15pt"
    p1.yaxis.axis_label_text_font_size = "15pt"
    p1.legend.label_text_font_size = "15pt"
    p1.xaxis.major_label_text_font_size = "12pt"


    show(column(p,p1))
    ### ------------------------------Residual before DTW
    residual_plot_name = params["node_60kV"]+"_residual.html"
    output_file(params['plots_loc']+residual_plot_name)
    p1 = figure(title='Residual Loads before DTW', x_axis_label='time', y_axis_label='Load [MW]', plot_width=1500)
    p1.line(range(N), residuals, line_color=color_q[1], legend='without DTW')
    p2 = figure(title='Residual Loads after DTW', x_axis_label='time', y_axis_label='Load [MW]', plot_width=1500)
    p2.line(range(N), residuals_DTW, line_color=color_q[2], legend='with DTW')
    p3 = figure(title='Residual Reactive after DTW', x_axis_label='time', y_axis_label='Load [MVAR]', plot_width=1500)
    p3.line(range(N), residual_DTW_reactive, line_color=color_q[3], legend='with DTW')
    p1.xaxis.axis_label_text_font_size = "15pt"
    p1.yaxis.axis_label_text_font_size = "15pt"
    p1.legend.label_text_font_size = "15pt"
    p2.xaxis.axis_label_text_font_size = "15pt"
    p2.yaxis.axis_label_text_font_size = "15pt"
    p2.legend.label_text_font_size = "15pt"
    p3.xaxis.axis_label_text_font_size = "15pt"
    p3.yaxis.axis_label_text_font_size = "15pt"
    p3.legend.label_text_font_size = "15pt"
    p1.xaxis.major_label_text_font_size = "12pt"
    p2.xaxis.major_label_text_font_size = "12pt"
    p3.xaxis.major_label_text_font_size = "12pt"
    show(column(p1, p2, p3))

    '''
    fig, ax = plt.subplots(figsize=(15,5))
    plt.plot(netvind_timestamps[0:10000],netvind_only_load_1[:10000], ls='-.', color='red', label='Netvind')
    plt.plot(netvind_timestamps[0:10000],solution_1_at10kV[:10000], ls=':',color='green', label='Simbench-wind-solar')
    plt.title(f'{node_name}')
    plt.legend(fontsize=15)
    plt.show()
    
    fig, ax = plt.subplots(figsize=(15,5))
    plt.plot(solar_pp[0:700], ls='--', label='Solar')
    if wind_pp.shape[1]>=1:
        plt.plot(wind_pp[0:700], ls=':', marker='.', label='Wind')
    plt.legend(fontsize=15)
    plt.title(f'{node_name}')
    plt.show()
    '''

    ############## Bar plot showing the contribution of each profile
    bar_plot_name = params["node_60kV"]+"_bar_chart.html"
    x = np.arange(1+num_of_wind_profiles,27+1+num_of_wind_profiles)
    output_file(params['plots_loc']+bar_plot_name)
    p1 = figure(title='', x_axis_label='Standard Load Profiles', y_axis_label='Demand [MW]', plot_height=500,plot_width=700)
    p1.vbar(x=0,top=sol1[0],bottom=0, width=0.6,  color=color_q[4],legend='Solar')
    p1.vbar(x=range(1,num_of_wind_profiles+1),top=sol1[1:num_of_wind_profiles+1],width=0.6, bottom=0, color=color_q[2],legend='Wind')
    sol_sim = np.array(sol1[num_of_wind_profiles+1:])
    idx = sim_classification == 'H'
    p1.vbar(x = x[idx], top=sol_sim[idx],width=0.6, bottom=0, legend='House', color=color_q[3])
    idx = sim_classification == 'L'
    p1.vbar(x=x[idx], top=sol_sim[idx],width=0.6, bottom=0, legend='Agri', color=color_q[1])
    idx = sim_classification == 'G'
    p1.vbar(x=x[idx], top=sol_sim[idx],width=0.6, bottom=0, legend='Commercial', color=color_q[0])
    idx = sim_classification == 'B'
    p1.vbar(x=x[idx], top=sol_sim[idx],width=0.6, bottom=0, legend='Misc.', color=color_q[5])
    ## Setting x-tick labels
    gen_names = ['PV']
    wind = ['Wind']*num_of_wind_profiles
    gen_names.extend(wind)
    gen_names.extend(sim_names)
    p1.xaxis.major_label_overrides = dict(zip(range(27+1+num_of_wind_profiles), gen_names))
    p1.xaxis.ticker = list(range(27+1+num_of_wind_profiles))
    p1.xaxis.axis_label_text_font_size = "15pt"
    p1.yaxis.axis_label_text_font_size = "15pt"
    p1.legend.label_text_font_size = "15pt"
    #p1.title.title_text_font_size = "15pt"
    p1.xaxis.major_label_text_font_size = "12pt"
    p1.xaxis.major_label_orientation = np.pi/2
    #p1.legend.text_font_size = "15pt"
    #p1.xaxis.ticker = gen_names
    show(p1)

    ############## Scatter plots
    scatter_plot_name = params["node_60kV"]+"_scatter.html"
    output_file(params['plots_loc']+scatter_plot_name)
    p1 = figure(title='Scatter plot without DTW', x_axis_label='Netvind profile [MW]', y_axis_label='Simbench Profile Solution [MW]', plot_height=600,plot_width=600)
    p1.circle(timeseries['netvind_MVA'],solution_1_at10kV-res, color=color_q[1])
    p2 = figure(title='Scatter plot with DTW', x_axis_label='Netvind profile [MW]', y_axis_label='Simbench Profile Solution [MW]', plot_height=600,plot_width=600)
    p2.circle(timeseries['netvind_MVA'],DTW_load_profile-res, color=color_q[2])
    p1.xaxis.axis_label_text_font_size = "15pt"
    p1.yaxis.axis_label_text_font_size = "15pt"
    #p1.legend.label_text_font_size = "15pt"
    p2.xaxis.axis_label_text_font_size = "15pt"
    p2.yaxis.axis_label_text_font_size = "15pt"
    p1.xaxis.major_label_text_font_size = "12pt"
    p2.xaxis.major_label_text_font_size = "12pt"
    #p2.legend.label_text_font_size = "15pt"
    show(row(p1, p2))






        #fig, ax = plt.subplots(figsize=(15,5))

        #plt.savefig('.\plots\DTW_200points.png')


        #################################### Saving to excel #########################



    wb = load_workbook('.\\results\\benchmark_model_ERA5.xlsx')
    ws = wb['Sheet1']
    names = ws['A']
    keep_looking = True
    k = 0
    while keep_looking:
        if names[k].value == params['node_60kV']:
            keep_looking = False
        else:
            k = k+1
    k = k+1
    ws['D'+str(k)].value = max(netvind_only_load_1)
    ws['E'+str(k)].value = min(netvind_only_load_1)
    ws['F'+str(k)].value = np.sqrt(solution1.objective_value)
    ws['G'+str(k)].value = corr_sol1[0]
    ws['H'+str(k)].value = max(solution_1_at10kV-res)
    ws['I'+str(k)].value = min(solution_1_at10kV-res)
    ws['J'+str(k)].value = 100*np.sqrt(solution1.objective_value)/(max(abs(max(netvind_only_load_1)), abs(min(netvind_only_load_1))))
    ws['K'+str(k)].value = RMSE_DTW
    ws['L'+str(k)].value = corr_DTW
    ws['M'+str(k)].value = max(DTW_load_profile-res)
    ws['N'+str(k)].value = min(DTW_load_profile-res)
    ws['O'+str(k)].value = 100*RMSE_DTW/(max(abs(max(netvind_only_load_1)), abs(min(netvind_only_load_1))))
    ws['P'+str(k)].value = max(residuals)
    ws['Q'+str(k)].value = min(residuals)
    ws['R'+str(k)].value = max(residuals_DTW)
    ws['S'+str(k)].value = min(residuals_DTW)
    ws['T'+str(k)].hyperlink = params['plots_loc']+timeseries_plot_name
    ws['T'+str(k)].value = 'open plot'
    ws['U'+str(k)].hyperlink = params['plots_loc']+bar_plot_name
    ws['U'+str(k)].value = 'open plot'
    ws['V'+str(k)].hyperlink = params['plots_loc']+scatter_plot_name
    ws['V'+str(k)].value = 'open plot'
    ws['W'+str(k)].hyperlink = params['plots_loc']+residual_plot_name
    ws['W'+str(k)].value = 'open plot'
    '''
    ws_stat = wb['Stats']
    names = ws_stat['A']
    keep_looking = True
    k = 0
    while keep_looking:
        if names[k].value == params['node_60kV']:
            keep_looking = False
        else:
            k = k + 1
    k = k + 1
    ws_stat['F'+str(k)] = load_cumulative_capacities[4,0]
    ws_stat['G'+str(k)] = load_cumulative_capacities[5,0]
    ws_stat['H'+str(k)] = load_cumulative_capacities[0,0]
    ws_stat['I'+str(k)] = load_cumulative_capacities[1,0]
    ws_stat['J'+str(k)] = load_cumulative_capacities[2,0]
    ws_stat['K'+str(k)] = load_cumulative_capacities[3,0]
    ws_stat['L'+str(k)].value = max(residuals_DTW)
    ws_stat['M'+str(k)].value = min(residuals_DTW)
    ws_stat['O'+str(k)] = len(category_assign_full)
    '''
    del names, keep_looking
    ws_profile = wb['simbenchProfileAllocation']
    names = ws_profile['B1:S1']
    keep_looking = True
    k = 0
    while keep_looking:
        if names[0][k].value == params['node_60kV']:
            keep_looking = False
        else:
            k = k+1
    k = ord('B')+k
    letter = chr(k)
    for i in range(len(sol1_round)):
        ws_profile[letter+str(i+2)].value = sol1_round[i]
    for k in range(len(sol_res)):
        ws_profile[letter+str(29+k)] = sol_res[k]
    # Finally save
    wb.save('.\\results\\benchmark_model_ERA5.xlsx')
    del timeseries
    flag_first_time = False

print('Done')



###########*****************************End of useful code*****************************************************************************************



'''
Model 2: Bounded according to level 
'''

#mdl2 = Model(name='Capacity_estimation_bounded')
''' These are not required here because the capacities are now part of the continuous scipy minimize problem and it requires a different bound format. 
# Bound 1 is on the maximum power for each simbench profile
lower_bound_1 = np.zeros(num_profiles_from_simbench + 2)
lower_bound_1[19:25] =  level_2_max- level_1_max
lower_bound_1[25:] =  level_3_max- level_2_max
lower_bound_1 = lower_bound_1.tolist()

upper_bound_1 = np.ones(num_profiles_from_simbench + 2)
upper_bound_1[0] = 0.5 # solar installation maximum size = 7kW
upper_bound_1[1] = 3 # distributed Wind maximum size 3MW
upper_bound_1[2:19] = level_1_max
upper_bound_1[19:25] =  level_2_max
upper_bound_1[25:] =  level_3_max
upper_bound_1 = upper_bound_1.tolist()
'''
'''
# bound 2 is for the number of profiles for each level.
lower_bound_2 = [0]*(num_profiles_from_simbench+2)
upper_bound_2 = np.zeros(num_profiles_from_simbench+2)
upper_bound_2[0:2] = 100#round(num_loads_dinemo/2)
upper_bound_2[2:19] = num_loads_dinemo #num_pro_lvl_1
upper_bound_2[19:25] = num_loads_dinemo #num_pro_lvl_2
upper_bound_2[25:] = num_loads_dinemo #num_pro_lvl_3
upper_bound_2 = upper_bound_2.tolist()

# cplex model now has integer variables only.
num_of_install = mdl2.integer_var_list(num_profiles_from_simbench + 2, lower_bound_2, upper_bound_2)
objective = lambda x: bounded_objective(mdl2, x, num_of_install, timeseries)
x0 = np.ones(num_profiles_from_simbench + 2) * min_load
## These are the bounds on capacities.
level_3_max = level_3_max*10
bound_gen = [(0,1), (0,3)]
bound_low = [(0,level_3_max)]*17
bound_med = [(0,level_3_max)]*6
#bound_med = [(abs(level_2_max-level_1_max),level_3_max)]*6
bound_high = [(0, level_3_max)]*4
bound_gen.extend(bound_low)
bound_gen.extend(bound_med)
bound_gen.extend(bound_high)

res = minimize(objective, x0, bounds=bound_gen, method='L-BFGS-B', tol=1e-6, options={'disp': True})

solution2 = bounded(mdl2, res.x, num_of_install, timeseries)

sol_1 = [solution1.get_value(coef[i]) for i in range(len(coef))]
sol_2 = [res.x[i]*solution2.get_value(num_of_install[i]) for i in range(len(num_of_install))]
num_install = [solution2.get_value(num_of_install[i]) for i in range(len(num_of_install))]
capacities_sol2 = res.x


data = {'sol_1': sol_1, 'sol_2': sol_2, 'capacities_sol2': capacities_sol2, 'num_install_sol2': num_install}
solution = {'data': data, 'timeseries': timeseries}
# finally save the solutions.
import  pickle
filename = '.\data_dump\sol_v3.pkl'
file = open(filename, 'wb')
pickle.dump(solution, file)
file.close()
'''
#'ORU', 'KRE','GLO','STY','VND','BLL','GNG','LAN','LMS','HOL','RAV',
# 'LOG', MGL, HDP
#node_names = [ 'KRE','GLO','STY','VND','BLL','GNG','LAN','LMS','HOL','RAV','RSM','DUR','SPK','SVL', 'EGR','EGB','RSL','HDP']
'''
node_names =['VND']
for node in node_names:
    print('*********************** START *************************\n')
    print(node)
    run_wrapper(node)
    print('********************** END  **************************\n')
'''
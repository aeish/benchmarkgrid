import numpy as np
from datetime import datetime
from docplex.mp.model import Model
from scipy.optimize import minimize

def unconstrained_unbounded(mdl, coef, params):
    netvind_MVA = params['netvind_MVA']
    wind_pp = params['wind_pp']
    solar_pp = params['solar_pp']
    simbench_load_profiles = params['simbench_load_profiles']
    num_profiles_from_simbench = simbench_load_profiles.shape[1]
    num_wind_profiles = wind_pp.shape[1]
    num_timestamps_NV = len(netvind_MVA)
    mdl.minimize(mdl.sumsq(
        (netvind_MVA[i] + coef[0] * solar_pp[i,0] + mdl.sum(coef[k+1] * wind_pp[i,k] for k in range(num_wind_profiles)) \
         - mdl.sum( coef[j + num_wind_profiles+1] * simbench_load_profiles[i, j] for j in range(num_profiles_from_simbench))) \
        for i in range(num_timestamps_NV))/num_timestamps_NV)
    # Netvind - (Load -generation)
    sol = mdl.solve(log_output=True)

    return sol

'''
Variables: 
1. Total solar installation capacity
2. Total wind installation capacity 
3. Maximum MVA per simbench profile 
'''
# Variables:

# capacity bounds

def bounded_objective(mdl, coef, num_of_install, params):
    netvind_MVA = params['netvind_MVA']
    wind_pp = params['wind_pp']
    solar_pp = params['solar_pp']
    simbench_load_profiles = params['simbench_load_profiles']
    num_profiles_from_simbench = simbench_load_profiles.shape[1]
    num_timestamps_NV = len(netvind_MVA)
    mdl.minimize(mdl.sumsq((netvind_MVA[i]+num_of_install[0]*coef[0]*solar_pp[i,0]+num_of_install[1]*coef[1]*wind_pp[i]\
                          -mdl.sum(num_of_install[j+2]*coef[j+2]*simbench_load_profiles[i,j] for j in range(num_profiles_from_simbench))) \
                        for i in range(num_timestamps_NV))/num_timestamps_NV)
    sol = mdl.solve(log_output=False)
    #print(f'Objective {sol.objective_value}')
    #sol.display()
    return sol.objective_value

def bounded(mdl, coef, num_of_install, params):
    netvind_MVA = params['netvind_MVA']
    wind_pp = params['wind_pp']
    solar_pp = params['solar_pp']
    simbench_load_profiles = params['simbench_load_profiles']
    num_profiles_from_simbench = simbench_load_profiles.shape[1]
    num_timestamps_NV = len(netvind_MVA)
    mdl.minimize(mdl.sumsq((netvind_MVA[i]+num_of_install[0]*coef[0]*solar_pp[i,0]+num_of_install[1]*coef[1]*wind_pp[i]\
                          -mdl.sum(num_of_install[j+2]*coef[j+2]*simbench_load_profiles[i,j] for j in range(num_profiles_from_simbench))) \
                        for i in range(num_timestamps_NV))/num_timestamps_NV)
    sol = mdl.solve(log_output=True)
    print(f'Objective {sol.objective_value}')
    #sol.display()
    return sol



from openpyxl import load_workbook
import math
import numpy as np
import pandas as pd


nameVsProfile = np.array(np.genfromtxt('..\\data\\name_to_profile.txt', delimiter=',', dtype=['S16, f8']).tolist())

node_60kV_name = ['GLO', 'ORU', 'HOL', 'KRE', 'BLL', 'LMS', 'SVL', 'LAN', 'STY', 'GNG', 'VND', 'SPK', 'EGR', 'RAV',
                  'RSM', 'RSL', 'DUR']
params = dict(node_60kV='GLO',  # node name
              data_loc='..\\data\\',  # all data location
              dinemo_loc='..\\data\\DiNeMo Data\\',  # location for DiNeMo Data
              corres_loc='..\\data\\corres_data\\',  # Location for CorRES data (not used here)
              netvind_loc='..\\data\\netvind_loads_and_network\\',  # Netvind data location
              data_to_save_loc='C:\\workingDirectory\\Programs\\benchmarkgrid\\results\\Data\\',
              plots_loc='C:\\workingDirectory\\Programs\\benchmarkgrid\\plots\\ERA5\\')  # location to save plots.

for node in node_60kV_name:
    params['node_60kV'] = node
    # Profile number:
    for i in nameVsProfile:
        if i[0][0].decode() == params['node_60kV']:
            profile_no = i[0][1].decode()[:2]
            break
    '''
    Load DiNeMo Stats- 
    '''
    dinemo_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+ 'CConsumer.csv'
    dinemo_loads_all = np.array(np.genfromtxt(dinemo_filename, dtype=['f8', 'f8'], delimiter=',', skip_header=True, usecols=[2,3]).tolist())
    LV_node_index = np.where(dinemo_loads_all[:, 0]==0.4)[0]                                     # index of all LV nodes -> 400V
    MV_node_index = np.where(dinemo_loads_all[:, 0] == 20)[0]
    dinemo_loads = dinemo_loads_all[LV_node_index, :]                                            # keep only LV nodes for assignment purpose.
    dinemo_loads[:, 1] = dinemo_loads[:, 1]/min(dinemo_loads[:, 1])                          # divide by the minimum load
    no_of_consumers = len(dinemo_loads)
    '''
    Load capacities allocated to each simbench load profile by optimization
    '''
    wb = load_workbook('.\\results\\benchmark_model_ERA5.xlsx')
    ws_profile = wb['simbenchProfileAllocation']
    names = ws_profile['C2:S2']
    keep_looking = True
    k = 0
    #names = ws_profile['B1:S1']
    while keep_looking:
        if names[0][k].value == params['node_60kV']:
            keep_looking = False
        else:
            k = k+1
    k = ord('C')+k
    letter = chr(k)
    profile_capacity = np.zeros(27)
    profile_name = []
    res_capacity = []
    for i in range(27):
        profile_capacity[i] = ws_profile[letter + str(i + 3)].value
        profile_name.append(ws_profile['A'+str(i + 3)].value)

    i = 30
    while ws_profile[letter + str(i)].value is not None:
        res_capacity.append(ws_profile[letter + str(i)].value)
        i = i+1
    res_capacity = np.array(res_capacity)
    profile_name = np.array(profile_name)

    '''
    Load Profile Assignment to 400V nodes. 
    '''
    min_sim_demand = sum(profile_capacity)/no_of_consumers # minimum value of simultaneous maximum demand in MW
    profile_name = profile_name[profile_capacity>min_sim_demand] # keep profiles with installed capacity above minimum demand. -> eliminates all zeros
    profile_capacity = profile_capacity[profile_capacity>min_sim_demand]
    cap_min = sum(profile_capacity)/sum(dinemo_loads[:,1])
    levels = np.unique(dinemo_loads[:,1])
    Nl_levels = np.array([sum(dinemo_loads[:,1]==a) for a in levels])

    no_cap_level = np.zeros([len(profile_capacity), 3])


    levels_cap = levels*Nl_levels

    N = len(profile_capacity)-1
    K = len(levels_cap)-1
    Cap_j = profile_capacity[N]
    flag_save = False
    _num_loads_ = []
    _level_ =[]
    while N>-1:
        _Nl_ = round(Cap_j/(levels[K]*cap_min))
        if _Nl_<=Nl_levels[K]:
            Nl_levels[K] = Nl_levels[K]-_Nl_
            _num_loads_.append(_Nl_)
            _level_.append(levels[K])
            flag_save = True
        else:  # _Nl_> Nl_levels[K]
            Cap_j = Cap_j - Nl_levels[K]*cap_min*levels[K]
            _num_loads_.append(Nl_levels[K])
            _level_.append(levels[K])
            Nl_levels[K] = 0
            flag_save = False
        if Nl_levels[K]==0:
            K = K-1
        if N == 0:
            _num_loads_ = [len(dinemo_loads)-sum(no_cap_level[:,0])]
            flag_save = True
        if flag_save:
            no_cap_level[N,0], no_cap_level[N,1] = sum(_num_loads_), profile_capacity[N]/sum(_num_loads_)
            N = N - 1
            Cap_j = profile_capacity[N]
            _num_loads_ = []
            _level_ = []
    no_cap_level[:, 2] = no_cap_level[:, 1]*math.acos(0.97)

    consumers = pd.read_csv(dinemo_filename)
    consumers.loc[MV_node_index, 'NVolt_kV'] = 10
    consumers.loc[MV_node_index, 'Dem_kVA'] = 0
    if len(consumers.columns) > 4:
        consumers.drop(consumers.columns[[-1]], axis=1, inplace=True)
    consumers.insert(4, 'mvar', np.zeros(len(dinemo_loads_all)))
    consumers.insert(5, 'bus_type', np.ones(len(dinemo_loads_all)))
    consumers.insert(6, 'slp', np.zeros(len(dinemo_loads_all)))
    sorted_loads = np.argsort(dinemo_loads[:, 1])
    slp = []
    MW_cap = []
    MVAR_cap = []

    for i in range(len(no_cap_level[:, 0])):
        slp.extend([profile_name[i]] * int(no_cap_level[i, 0]))
        MW_cap.extend([no_cap_level[i, 1]] * int(no_cap_level[i, 0]))
        MVAR_cap.extend([no_cap_level[i, 2]] * int(no_cap_level[i, 0]))

    slp, MW_cap, MVAR_cap = np.array(slp), np.array(MW_cap), np.array(MVAR_cap)
    slp, MW_cap, MVAR_cap = slp[sorted_loads], MW_cap[sorted_loads], MVAR_cap[sorted_loads]
    consumers.loc[LV_node_index, 'Dem_kVA'] = MW_cap
    consumers.loc[LV_node_index, 'mvar'] = MVAR_cap
    consumers.loc[LV_node_index, 'slp'] = slp
    node_names = consumers.Node.values
    if node == 'LAN':
        name: str = 'CMT0  '
    else:
        name: str = 'CMT0   '
    idx = np.where(node_names == name)[0]
    consumers.loc[idx[0], 'bus_type'] = 1
    consumers.loc[idx[0], 'slp'] = 'residual'
    consumers.loc[idx[0], 'Dem_kVA'] = 1
    consumers.loc[idx[0], 'mvar'] = 1
    MV_i = 1
    '''
    Load Solar stats from the workbook
    1. 25% of the total household consumption is satisfied by PV
    2. 40% of the total Agri+Commercial is PV 
    3. Rest miscellaneous or at MV node. 
    '''

    if len(res_capacity)>0:
        total_pv = res_capacity[0]
        H_solar = sum(profile_capacity[0:5]) * 0.25
        GL_solar = sum(profile_capacity[5:]) * 0.4
        MV_solar = 0
        if GL_solar > total_pv:
            GL_solar = total_pv
            H_solar = 0
        elif GL_solar+H_solar > total_pv:
            H_solar = total_pv-GL_solar
        else:
            MV_solar = total_pv - H_solar - GL_solar
        if MV_solar <0:
            MV_solar = 0
        ''' Loading PV statistics'''
        ws_pv = wb['PV']
        xx_min = ws_pv['B1:R1'][0]
        xx_max = ws_pv['B2:R2'][0]
        xx_installed = ws_pv['B3:R3'][0]
        solar_stat = np.zeros([len(xx_min), 3]) # array contains statistics from the excel sheet.
        for i in range(len(xx_min)):
            solar_stat[i, 0], solar_stat[i, 1], solar_stat[i, 2] = xx_min[i].value/1000, xx_max[i].value/1000, xx_installed[i].value
        solar_stat[:,2] = solar_stat[:,2]/sum(solar_stat[:,2]) # calculate percentage of each pv installation.
        del xx_min, xx_max, xx_installed, ws_pv

        flag_continue_distributing_pv = True

        pv_distribution = np.array([H_solar, GL_solar])
        pv_distribution = pv_distribution[pv_distribution>0]
        pv_capacities = no_cap_level[:,1] * 0.8 # np.random.uniform(low=0.4, high=0.8, size=(len(load_capacity))) # 40-80% of maximum demand in PV
        pv_capacities[pv_capacities<0.001] = 0.0011 # minimum PV installation is 1kW

        pv_installation = np.zeros([len(profile_capacity), 2])
        xx_pv_percentage = np.zeros(len(profile_capacity))
        for i in range(len(profile_capacity)):
            xx =  np.logical_and(solar_stat[:,0]<=pv_capacities[i] ,solar_stat[:,1]>pv_capacities[i])
            xx_pv_percentage[i] = solar_stat[xx,2]
            pv_capacities[i] = solar_stat[xx,0]
        xx_pv_percentage[:5] = H_solar*xx_pv_percentage[:5]/ sum(xx_pv_percentage[:5])
        xx_pv_percentage[5:] = GL_solar * xx_pv_percentage[5:] / sum(xx_pv_percentage[5:])
        pv_installation[:, 0] = pv_capacities
        pv_installation[:, 1] = np.round(xx_pv_percentage/pv_capacities)
        MV_solar = MV_solar + (H_solar - sum(pv_installation[:5,0]*pv_installation[:5,1]))+ (GL_solar - sum(pv_installation[5:,0]*pv_installation[5:,1]))
        if MV_solar <0:
            MV_solar = 0


        pv_cap_at400V = np.zeros(len(LV_node_index))
        bus_type = np.ones(len(LV_node_index))
        kk = 0
        for i in range(len(no_cap_level[:, 0])):
            if no_cap_level[i,0]<pv_installation[i,1]:
                pv_installation[i,0] = (pv_installation[i, 0]*pv_installation[i, 1])/no_cap_level[i,0]
                pv_installation[i,1] = no_cap_level[i, 0]
            bus_type[kk:int(kk+pv_installation[i, 1])] = 2
            pv_cap_at400V[kk:int(kk + pv_installation[i, 1])] = pv_installation[i, 0]
            kk = int(kk + no_cap_level[i, 0])
        pv_cap_at400V, bus_type = pv_cap_at400V[sorted_loads], bus_type[sorted_loads]


        consumers.insert(7, 'pv_cap', np.zeros(len(dinemo_loads_all)))
        consumers.loc[LV_node_index, 'bus_type'] = bus_type
        consumers.loc[LV_node_index, 'pv_cap'] = pv_cap_at400V

        del bus_type
        bus_type = consumers.bus_type.values
        gen = consumers.iloc[bus_type==2, :].copy()
        gen_type = np.array(['PV']*len(gen))
        consumers.drop('pv_cap', axis=1, inplace=True)
        gen.drop(columns = ['Dem_kVA','mvar','bus_type', 'slp'], inplace=True)
        gen.insert(4, 'gen_type', gen_type)
        gen.rename(columns={'pv_cap':'mw'}, inplace=True)
        consumers.rename(columns={'Dem_kVA': 'mw'}, inplace=True)


        if sum(pv_installation[:,1]) > no_of_consumers:
            error = error+1
        print(node)
        print('Number of MV nodes:'+str(len(MV_node_index)))
        print('Wind:'+str(sum(res_capacity[1:]))+' No of wind profiles')

        if len(res_capacity)>1:
            no_of_wind_installations = int(sum(np.ceil(res_capacity[1:]/3.6)))
            wind_per_profile = np.ceil(res_capacity[1:]/3.6)
            res_temp = res_capacity.copy()
            wind_cap = np.zeros(no_of_wind_installations)
            wind_idx = 1
            for hh in range(no_of_wind_installations):
                if res_temp[wind_idx]<=0:
                    wind_idx = wind_idx+1
                if res_temp[wind_idx]>3.6:
                    wind_cap[hh] = 3.6
                    res_temp[wind_idx] = res_temp[wind_idx]-3.6
                else:
                    wind_cap[hh] = res_capacity[wind_idx]
                    res_temp[wind_idx] = 0


        else:
            no_of_wind_installations = 0
        print('Gen at MV: ' + str(no_of_wind_installations+1))

        MV_i = 1
        if no_of_wind_installations+1<len(MV_node_index):
            if MV_solar>0:
                name = name.replace('0', str(MV_i))
                idx = np.where(node_names == name)[0]
                consumers.loc[idx[0], 'bus_type'] = 2
                consumers.loc[idx[0], 'slp'] = '0'
                consumers.loc[idx[0], 'mw'] = 0
                consumers.loc[idx[0], 'mvar'] = 0
                gen_row = dict(Code = consumers.loc[idx[0],'Code'], Node=consumers.loc[idx[0],'Node'],
                               NVolt_kV = consumers.loc[idx[0],'NVolt_kV'], mw=MV_solar, gen_type='PV')
                gen = gen.append(gen_row, ignore_index=True)
                MV_i = 2
            if no_of_wind_installations>0:
                wind_profile = 1
                for hh in range(no_of_wind_installations):
                    name = name.replace(str(MV_i-1), str(MV_i))
                    idx = np.where(node_names == name)[0]
                    consumers.loc[idx[0], 'bus_type'] = 2
                    consumers.loc[idx[0], 'slp'] = '0'
                    consumers.loc[idx[0], 'mw'] = 0
                    consumers.loc[idx[0], 'mvar'] = 0
                    if hh == sum(wind_per_profile[:wind_profile]):
                        wind_profile = wind_profile+1
                    gen_row = dict(Code=consumers.loc[idx[0], 'Code'], Node=consumers.loc[idx[0], 'Node'],
                                   NVolt_kV=consumers.loc[idx[0], 'NVolt_kV'], mw=wind_cap[hh], gen_type='WPP'+str(wind_profile))
                    gen = gen.append(gen_row, ignore_index=True)
                    MV_i = MV_i +1
        elif len(MV_node_index)==1:
            idx = np.where(node_names == 'CMT0  ')[0]
            consumers.loc[idx[0], 'bus_type'] = 2
            consumers.loc[idx[0], 'slp'] = 'residual'
            consumers.loc[idx[0], 'mw'] = 1
            consumers.loc[idx[0], 'mvar'] = 1

            gen_row = dict(Code=consumers.loc[idx[0], 'Code'], Node=consumers.loc[idx[0], 'Node'],
                           NVolt_kV=consumers.loc[idx[0], 'NVolt_kV'], mw=MV_solar, gen_type='PV')
            gen = gen.append(gen_row, ignore_index=True)
            gen_row = dict(Code=consumers.loc[idx[0], 'Code'], Node=consumers.loc[idx[0], 'Node'],
                           NVolt_kV=consumers.loc[idx[0], 'NVolt_kV'], mw=res_capacity[1],
                           gen_type='WPP1' )
            gen = gen.append(gen_row, ignore_index=True)
            print(node+' special case')
        gen_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+'gen.csv'
        #gen.to_csv(gen_filename, index=False)
    consumer_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+'load.csv'
    #consumers.to_csv(consumer_filename, index=False)



        #if no_of_wind_installations+1 > len(MV_node_index):
        #    gen_nodes = len(MV_node_index)
        #else:
        #    gen_nodes = no_of_wind_installations+1



'''
sol1_round = sol1_round[sol1_round!=0]

cat_available = np.array([i[-1] for i in sim_names_arr]) # last letter in the profile name suggests category
cat_unique = ['A', 'B', 'C', 'G', 'M', 'H'] # these are all the categories available
#calculate percentages of each category -> inline for loop
percentage_each_category = [round(sum(cat_available==i)/len(cat_available), 2) for i in cat_unique]

########### Load statistics from DiNeMo

dinemo_loads = np.array(np.genfromtxt(dinemo_filename, dtype=['f8', 'f8'], delimiter=',', skip_header=True, usecols=[2,3]).tolist())
LV_node_index = np.where(dinemo_loads[:,0]==0.4)[0] # index of all LV nodes -> 400V
dinemo_loads = dinemo_loads[LV_node_index,:] # keep only LV nodes for assignment purpose.
dinemo_loads[:,1] = dinemo_loads[:,1]/min(dinemo_loads[:,1]) # divide by the minimum load
no_of_consumers = len(dinemo_loads)
# generate array with categories as % of number of consumers.
temp = [cat_unique[-1]]*int(round(percentage_each_category[-1]*no_of_consumers))
for i in range(1,5):
    f = [cat_unique[-1-i]]*int(round(percentage_each_category[-1-i]*no_of_consumers))
    f.extend(temp)
    temp = f
category_assignment = [cat_unique[0]]*int(no_of_consumers-len(f))
category_assignment.extend(f)
category_assignment = np.array(category_assignment)
del f, temp
category_assign_full = []
category_capacity = []
#### SORT LEVEL 2:
for i in cat_unique:
    sub_cats = sim_names_arr[cat_available == i].tolist()
    sub_cat_capacity = sol1_round[cat_available==i]
    if len(sub_cats) == 0:
        #do nothing
        temp = []
        temp_capacity = []
    elif len(sub_cats) == 1:
        x = sum(category_assignment==i)
        temp = sub_cats*x
        temp_capacity = [sub_cat_capacity[0]/x]*x
    else:
        consumer_per_sub_cat = sum(category_assignment==i)*sol1_round[cat_available==i]/sum(sol1_round[cat_available==i])
        num_consumers_in_cat_i = sum(category_assignment==i)
        temp = []
        temp_capacity = []
        for k in range(len(sub_cats)-1):
            x = int(floor(consumer_per_sub_cat[k])) # x is the number of times the sub category is repeated.
            if x == 0:
                x = 1
            temp.extend([sub_cats[k]]*x)
            temp_capacity.extend([sub_cat_capacity[k]/x] * x)
        x = num_consumers_in_cat_i-len(temp)
        temp.extend([sub_cats[-1]]*x)
        temp_capacity.extend([sub_cat_capacity[-1] / x] * x)
    category_assign_full.extend(temp)
    category_capacity.extend(temp_capacity)
######### 4. argsort DiNeMo loads.
dinemo_ascending_index = np.argsort(dinemo_loads[:,1])
category_capacity = np.array(category_capacity)
category_assign_full = np.array(category_assign_full)
category_assign_full = category_assign_full[dinemo_ascending_index]
category_capacity = category_capacity[dinemo_ascending_index]
category_capacity_reactive = math.acos(0.97)*category_capacity

unique_cats_in_10kV = np.unique(category_assign_full)
load_cumulative_capacities = np.zeros((6,1))
for cat in unique_cats_in_10kV:
    if cat[0] == 'H':
        load_cumulative_capacities[0] = load_cumulative_capacities[0]+np.sum(category_capacity[category_assign_full==cat])
    elif cat[0]=='L':
        load_cumulative_capacities[1] = load_cumulative_capacities[1] + np.sum(
            category_capacity[category_assign_full == cat])
    elif cat[0] == 'G':
        load_cumulative_capacities[2] = load_cumulative_capacities[2] + np.sum(
            category_capacity[category_assign_full == cat])
    else:
        load_cumulative_capacities[3] = load_cumulative_capacities[3] + np.sum(
            category_capacity[category_assign_full == cat])
load_cumulative_capacities[4]=round(sol1[0],4)
load_cumulative_capacities[5]= round(sum(sol1[1:num_of_wind_profiles+1]),4)




dinemo_filename = params['dinemo_loc'] + params['node_60kV'] + '\\'+ 'CConsumers.csv' # Data for DiNeMo.
fields = []
    csv_rows = []
    with open(dinemo_filename, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        fields = next(csvreader)
        fields[3] = 'Dem_MW'
        fields[4] = 'Dem_MVAR'
        fields.append('Category')
        i = 0
        k = 0
        for file_row in csvreader:
            csv_rows.append(file_row)
            if float(csv_rows[i][2]) == 0.40:
                csv_rows[i][3] = category_capacity[k]
                csv_rows[i][4] = category_capacity_reactive[k]
                csv_rows[i].append(category_assign_full[k])
                k = k+1
            else:
                csv_rows[i][4] = '0.0'
                csv_rows[i][3] = '0.0'
                csv_rows[i].append('MV')
            i = i+1
    filename = params['dinemo_loc'] + params['node_60kV'] +'\\'+ 'load.csv'
    with open(filename, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(fields)
        csvwriter.writerows(csv_rows)
'''
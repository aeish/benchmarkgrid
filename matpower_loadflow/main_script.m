
%addpath('C:/workingDirectory/programs/benchmarkgrid/results/Data/'); 
%params = struct('base_mva', 100, 'expansion_nodes',ones(1,1)*27);
gen = readmatrix('gen.csv');
brch = readmatrix('line.csv');
bus = readmatrix('bus.csv');
gen(:,1)= gen(:,1)+1; 
bus(:,1) = bus(:,1)+1; 
brch(:,1) = brch(:,1)+1; 
brch(:,2) = brch(:,2)+1; 
base_MVA = 100; 
%% MATPOWER Case Format : Version 2
mpc.version = '2';

%%-----  Power Flow Data  -----%%
%% system MVA base
mpc.baseMVA = 100;
mpc.gen = gen; 
mpc.bus = bus; 
mpc.branch = brch; 

mpc_ext = loadcase(mpc); 
mpc_int = ext2int(mpc_ext);

mpopt  =  mpoption('out.all',  1,  'verbose',  0,  'pf.enforce_q_lims',1);
%mpopt  =  mpoption('out.all',  0,  'verbose',  0);
results = runpf(mpc_ext,mpopt);